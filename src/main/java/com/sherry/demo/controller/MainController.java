package com.sherry.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sherry.demo.entity.Student;
import com.sherry.demo.service.StudentsService;

@RestController
public class MainController {
	
	@Autowired
	StudentsService studentsService;
	
//	// 初始化(產生假資料)
//	@RequestMapping(value = "/init", method = RequestMethod.POST)
//	public ResponseEntity<List<Student>> initStudent() {
//		studentsService.generateStudent();
//		return new ResponseEntity<List<Student>>(studentsService.getStudent(), HttpStatus.CREATED);
//	}
	
	// update
	@RequestMapping(value = "/student", method = RequestMethod.PUT)
	public ResponseEntity<Student> updateStudent(@RequestBody Student stu) {
		Student student = studentsService.getStudentById(stu.getId());
		student.setName(stu.getName());
		student.setScore(stu.getScore());
		studentsService.updateStudent(stu.getId(), student);
		return new ResponseEntity<Student>(studentsService.getStudentById(stu.getId()), HttpStatus.OK);
	}
	
	// delete by id
	@RequestMapping(value = "/student/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteStudent(@PathVariable int id){
		studentsService.deleteStudent(id);
		return new ResponseEntity<>("delete id="+ id +" success", HttpStatus.OK);
	}
	
	// get by id
	@RequestMapping(value = "/student/{id}", method = RequestMethod.GET)
	public ResponseEntity<Student> getStudentById(@PathVariable int id){
		return new ResponseEntity<Student>(studentsService.getStudentById(id), HttpStatus.OK);
	}
	
	// get
	@RequestMapping(value = "/student", method = RequestMethod.GET)
	public ResponseEntity<List<Student>> getStudent() {
//		List<Student> students = new ArrayList<Student>();
//		for(int i = 1; i <= 10; i++) {
////			Student student = new Student();
////			student.setName(new String(RandomString.make(3)));
////			student.setScore(String.valueOf(new Random().nextInt(100)));
//			
////			students.add(new Student(RandomString.make(3), String.valueOf(new Random().nextInt(100))));
//			
//			String name = RandomString.make(10);
//			String score = String.valueOf(new Random().nextInt(100));
//			students.add(new Student(i, name, score));
//		}
//		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
		
		return new ResponseEntity<List<Student>>(studentsService.getStudent(), HttpStatus.OK);
	}
	
	// insert
	@RequestMapping(value = "/student", method = RequestMethod.POST)
	public ResponseEntity<?> getStudent(@RequestBody Student stu) {
		if(stu.getName().equals("test") && stu.getScore().equals("100")) {
			studentsService.insertStudent(stu);
			return new ResponseEntity<Student>(stu, HttpStatus.CREATED);
		} else {
//			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			
//			APIResult result = new APIResult();
//			result.setStatus(HttpStatus.FORBIDDEN.toString());
//			result.setMessage("insert ERROR!");
//			return new ResponseEntity<APIResult>(result, HttpStatus.FORBIDDEN);
			
			return new ResponseEntity<>(new Exception("insert ERROR!").getMessage(), HttpStatus.FORBIDDEN);
		}
	}

}
