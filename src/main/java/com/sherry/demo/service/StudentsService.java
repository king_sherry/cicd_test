package com.sherry.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.sherry.demo.entity.Student;

import net.bytebuddy.utility.RandomString;

@Service
public class StudentsService {

//	public List<Student> getStudent() {
//		List<Student> students = new ArrayList<Student>();
//		for(int i = 1; i <= 10; i++) {
////			Student student = new Student();
////			student.setName(new String(RandomString.make(3)));
////			student.setScore(String.valueOf(new Random().nextInt(100)));
//			
////			students.add(new Student(RandomString.make(3), String.valueOf(new Random().nextInt(100))));
//			
//			String name = RandomString.make(10);
//			String score = String.valueOf(new Random().nextInt(100));
//			students.add(new Student(i, name, score));
//		}
//		return students;
//	}

	private List<Student> students;

	public StudentsService() {
		List<Student> students = new ArrayList<Student>();
		for (int i = 1; i <= 10; i++) {
			String name = RandomString.make(10);
			String score = String.valueOf(new Random().nextInt(100));
			students.add(new Student(i, name, score));
		}
		this.students = students;
	}

	// 產生假資料
//	public void generateStudent() {
//		List<Student> students = new ArrayList<Student>();
//		for (int i = 1; i <= 10; i++) {
//			String name = RandomString.make(10);
//			String score = String.valueOf(new Random().nextInt(100));
//			students.add(new Student(i, name, score));
//		}
//		this.students = students;
//	}

	public List<Student> getStudent() {
		return students;
	}

	public void insertStudent(Student student) {
		students.add(student);
	}

	public Student getStudentById(int id) {
		return students.get(id - 1);
	}

	public void deleteStudent(int id) {
		students.remove(id - 1);
	}

	public void updateStudent(int id, Student student) {
		students.set(id - 1, student);
	}

}
